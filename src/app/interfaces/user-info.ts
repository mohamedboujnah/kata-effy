import { LodgmentEnum } from "src/app/enums/lodgment.enum";

export interface UserInfo {
    civility?: string;
    lastname?: string;
    firstname?: string;
    email?: string;
    tel?: string;
    lodgment?: LodgmentEnum,
    numberOfPersons?: number,
    income?: number;
    surface?: number;
}
