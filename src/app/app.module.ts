import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EstimateFormModule } from './pages/estimate-form/estimate-form.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   EstimateFormModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
