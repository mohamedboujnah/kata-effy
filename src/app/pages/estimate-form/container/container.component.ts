import { Component } from '@angular/core';
import { UserInfo } from '../../../interfaces/user-info';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent {

  public userInfo: UserInfo = {};
  public step = 1;

  constructor() { }

  public submitForm(info: UserInfo): void {
    this.userInfo = { ...this.userInfo, ...info };
    this.step += 1;
  }

  public restimate() {
    this.step = 1;
  }

}
