import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerComponent } from './container.component';
import { UserInfo } from '../../../interfaces/user-info';

describe('ContainerComponent', () => {
  let component: ContainerComponent;
  let fixture: ComponentFixture<ContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update userInfo and step when submitForm is called', () => {
    const info: UserInfo = {
      civility: 'Mr',
      lastname: 'Smith',
      firstname: 'John',
      email: 'john.smith@example.com',
      tel: '1234567890'
    };

    component.submitForm(info);

    expect(component.userInfo).toEqual(info);
    expect(component.step).toBe(2);
  });
  
});
