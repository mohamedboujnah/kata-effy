import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstimaeFormRoutingModule } from './estimate-form-routing.module';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ContainerComponent } from './container/container.component';
import { ProjectInfoComponent } from './components/project-info/project-info.component';
import { SummaryComponent } from './components/summary/summary.component';


@NgModule({
  declarations: [
    UserInfoComponent,
    ContainerComponent,
    ProjectInfoComponent,
    SummaryComponent
  ],
  imports: [
    CommonModule,
    EstimaeFormRoutingModule,
    ReactiveFormsModule
  ]
})
export class EstimateFormModule { }
