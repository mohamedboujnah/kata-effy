import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserInfo } from '../../../../interfaces/user-info';
import { LodgmentEnum } from 'src/app/enums/lodgment.enum';

@Component({
  selector: 'app-project-info',
  templateUrl: './project-info.component.html',
  styleUrls: ['./project-info.component.scss']
})
export class ProjectInfoComponent {

  @Output() submitForm = new EventEmitter<UserInfo>();

  public isSubmitted = false;
  public projectForm: FormGroup;
  public lodgmentEnum = LodgmentEnum;

  constructor() {
    this.projectForm = new FormGroup(
      {
        lodgment: new FormControl('', [Validators.required]),
        numberOfPersons: new FormControl('', [Validators.required]),
        income: new FormControl('', [Validators.required]),
        surface: new FormControl('', [Validators.required])
      }
    )
  }

  public checkError(field: string): boolean {
    return this.isSubmitted && this.projectForm.get(field)?.errors !== null;
  }

  public validateStep(form: FormGroup): void {
    this.isSubmitted = true;
    if (form.valid) {
      this.submitForm.emit({
        lodgment: form.get('lodgment')?.value,
        numberOfPersons: form.get('numberOfPersons')?.value,
        income: form.get('income')?.value,
        surface: form.get('surface')?.value,
      })
    }
  }

}
