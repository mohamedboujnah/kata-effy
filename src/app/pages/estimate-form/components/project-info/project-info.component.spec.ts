import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectInfoComponent } from './project-info.component';
import { LodgmentEnum } from 'src/app/enums/lodgment.enum';

describe('ProjectInfoComponent', () => {
  let component: ProjectInfoComponent;
  let fixture: ComponentFixture<ProjectInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ ProjectInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit submitForm event when form is valid', () => {
    spyOn(component.submitForm, 'emit');

    const lodgment = LodgmentEnum.LODGER;
    const numberOfPersons = 3;
    const income = 50000;
    const surface = 100;

    component.projectForm.patchValue({
      lodgment: lodgment,
      numberOfPersons: numberOfPersons,
      income: income,
      surface: surface
    });

    component.validateStep(component.projectForm);

    expect(component.submitForm.emit).toHaveBeenCalledWith({
      lodgment: lodgment,
      numberOfPersons: numberOfPersons,
      income: income,
      surface: surface
    });
  });

  it('should not emit submitForm event when form is invalid', () => {
    spyOn(component.submitForm, 'emit');
    component.validateStep(component.projectForm);
    expect(component.submitForm.emit).not.toHaveBeenCalled();
  });

  it('should return true when checkError is called with an invalid field', () => {
    component.isSubmitted = true;
    component.projectForm.get('lodgment')?.setErrors({ required: true });
    const result = component.checkError('lodgment');
    expect(result).toBeTrue();
  });

  it('should return false when checkError is called with a valid field', () => {
    component.isSubmitted = true;
    component.projectForm.get('lodgment')?.setErrors(null);
    const result = component.checkError('lodgment');
    expect(result).toBeFalse();
  });

  
});
