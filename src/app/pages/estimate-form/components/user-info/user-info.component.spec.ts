import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserInfoComponent } from './user-info.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('UserInfoComponent', () => {
  let component: UserInfoComponent;
  let fixture: ComponentFixture<UserInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ UserInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit submitForm event when form is valid', () => {
    spyOn(component.submitForm, 'emit');

    const formData = {
      civility: 'Mr',
      lastname: 'Smith',
      firstname: 'John',
      email: 'john.smith@example.com',
      tel: '1234567890'
    };

    component.userInfoForm.patchValue(formData);
    component.validateStep(component.userInfoForm);

    expect(component.submitForm.emit).toHaveBeenCalledWith(formData);
  });

  it('should not emit submitForm event when form is invalid', () => {
    spyOn(component.submitForm, 'emit');
    component.validateStep(component.userInfoForm);
    expect(component.submitForm.emit).not.toHaveBeenCalled();
  });

  it('should return true when checkError is called with an invalid field', () => {
    component.isSubmitted = true;
    component.userInfoForm.get('lastname')?.setErrors({ required: true });
    const result = component.checkError('lastname');
    expect(result).toBeTrue();
  });

  it('should return false when checkError is called with a valid field', () => {
    component.isSubmitted = true;
    component.userInfoForm.get('lastname')?.setErrors(null);
    const result = component.checkError('lastname');
    expect(result).toBeFalse();
  });

});
