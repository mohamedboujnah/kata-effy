import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserInfo } from '../../../../interfaces/user-info';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent {

  @Output() submitForm = new EventEmitter<UserInfo>();

  public userInfoForm: FormGroup;
  public isSubmitted = false;

  constructor() {
    this.userInfoForm = new FormGroup(
      {
        civility: new FormControl('', [Validators.required]),
        lastname: new FormControl('', [Validators.required]),
        firstname: new FormControl('', [Validators.required]),
        email: new FormControl('', [Validators.required, Validators.email]),
        tel: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(10), Validators.maxLength(11)]),
      }
    )
  }

  public checkError(field: string): boolean {
    return this.isSubmitted && this.userInfoForm.get(field)?.errors !== null;
  }

  public validateStep(form: FormGroup): void {
    this.isSubmitted = true;
    if (form.valid) {
      this.submitForm.emit({
        lastname: form.get('lastname')?.value,
        firstname: form.get('firstname')?.value,
        email: form.get('email')?.value,
        civility: form.get('civility')?.value,
        tel: form.get('tel')?.value,
      })
    }
  }

}
