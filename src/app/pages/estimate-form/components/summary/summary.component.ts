import { Component, Input } from '@angular/core';
import { UserInfo } from '../../../../interfaces/user-info';
import { LodgmentEnum } from 'src/app/enums/lodgment.enum';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent {

  @Input() userInfo: UserInfo;

  constructor() { }

  public get isNotOwner(): boolean {
    return this.userInfo.lodgment !== LodgmentEnum.OWNER;
  }

  public get effyPrice(): number{
    const projectPrice = this.userInfo.surface ? this.userInfo.surface * 80 : 0;
    return (projectPrice * 0.75) - (
      (this.userInfo.income ? this.userInfo.income : 0) / 
      (this.userInfo.numberOfPersons ? this.userInfo.numberOfPersons : 0)
    ) * 0.15
  }

}
