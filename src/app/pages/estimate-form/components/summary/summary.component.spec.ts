import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryComponent } from './summary.component';
import { LodgmentEnum } from 'src/app/enums/lodgment.enum';

describe('SummaryComponent', () => {
  let component: SummaryComponent;
  let fixture: ComponentFixture<SummaryComponent>;

  const civility = 'Mr';
  const firstname = 'TestFirstName';
  const lastname = 'TestLAstName';

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryComponent);
    component = fixture.componentInstance;
    component.userInfo = {
      civility: civility,
      firstname: firstname,
      lastname: lastname
    };
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return true for isNotOwner when the lodgment is not OWNER', () => {
    component.userInfo = { lodgment: LodgmentEnum.LODGER, numberOfPersons: 2, income: 30000, surface: 70 };
    fixture.detectChanges();
    expect(component.isNotOwner).toBeTrue();
  });

  it('should return false for isNotOwner when the lodgment is OWNER', () => {
    component.userInfo = { lodgment: LodgmentEnum.OWNER, numberOfPersons: 2, income: 30000, surface: 70 };
    fixture.detectChanges();
    expect(component.isNotOwner).toBeFalse();
  });

  it('should be eligible', () => {
    component.userInfo = { lodgment: LodgmentEnum.OWNER, numberOfPersons: 1, income: 15000, surface: 48 };
    fixture.detectChanges();
    expect(component.effyPrice).toBeGreaterThan(0);

  });

  it('should not be eligible', () => {
    component.userInfo = { lodgment: LodgmentEnum.OWNER, numberOfPersons: 1, income: 38000, surface: 48 };
    fixture.detectChanges();
    expect(component.effyPrice).toBeLessThan(0);
  });
  
});
